<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ){
	return;
}

?>

<?php foreach ( $messages as $message ) : ?>
	<div class="col-sm-12 col-xs-12 lert-remove woocommerce-notice-message xs-margin-bottom-10px"><div class="alert alert-info fade in" role="alert"><i class="fas fa-info-circle alert-info"></i> <span><?php echo wc_kses_notice( $message ); ?></span><button aria-hidden="true" data-dismiss="alert" class="close checkout-alert-remove" type="button">&times;</button></div></div>
<?php endforeach; ?>

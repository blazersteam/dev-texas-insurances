<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ){
	return;
}
?>
<div class="woocommerce-message" role="alert">
<?php foreach ( $messages as $message ) : ?>
	<div class="col-sm-12 col-xs-12 woocommerce-success-message alert-remove xs-margin-bottom-10px">
		<div class="alert alert-success fade in" role="alert">
			<i class="fas fa-thumbs-up alert-success"></i>
			<?php echo wc_kses_notice( $message ); ?>
			<button class="close checkout-alert-remove" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	</div>
<?php endforeach; ?>
</div>